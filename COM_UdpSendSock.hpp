#ifndef UDPSENDSOCK_H
#define UDPSENDSOCK_H

#include "COM.hpp"

namespace Ui {
    class UdpReceiveSock;
}

class UdpSendSock : public COM
{
    Q_OBJECT

public:
    UdpSendSock();
    virtual bool open(QString) override;
    virtual int close(void) override;

public slots:
    virtual int send(CANmessage) override;

private:
    QByteArray datagram;
    QUdpSocket *udpsocket;
    QString IP4Address;
    quint16 Port;
    int count;

signals:
    void new_messages(CANmessage);
    void closing(void);
};

#endif // UDPSENDSOCK_H
