#include "COM_UdpReceiveSock.hpp"

UdpReceiveSock::UdpReceiveSock()
{

}

bool UdpReceiveSock::open(QString port)
{
    UdpSocket = new QUdpSocket;
    UdpSocket->bind(port.section(":",1,1).toUShort(),QUdpSocket::ShareAddress);
    qDebug()<<port.section(":",0,0);
    qDebug()<<QString::number(port.section(":",1,1).toUShort());

    connect(UdpSocket,&QUdpSocket::readyRead,
            this,&UdpReceiveSock::read);
    qDebug("yes");
    return true;
}

void UdpReceiveSock::read(void)
{
    while (UdpSocket->hasPendingDatagrams()) {
        datagram.clear();
        datagram.resize(int(UdpSocket->pendingDatagramSize()));
        UdpSocket->readDatagram(datagram.data(),datagram.size());
        CANmessage Msg;
        Msg.candata.sid = datagram.left(8).toInt(nullptr,16);
        Msg.candata.dev_id = Msg.candata.sid & 0x1F;
        Msg.candata.msg_id = Msg.candata.sid >> 5;
        Msg.candata.dlc = datagram.mid(12,1).toInt();

        char aux[8];
        for (int i = 0;i < Msg.candata.dlc;i++) {
            aux[i] = datagram.mid(16+3*i,2).toInt(nullptr,16);

        }

        for (int i=0; i<4; i++)
            Msg.candata.data[i] = aux[2*i] + (aux[2*i + 1] << 8);

        Msg.timestamp = QString(datagram).section("@",1,1).toInt();
        emit new_messages(Msg);
    }
}

int UdpReceiveSock::send(CANmessage msg)
{

}

int UdpReceiveSock::close()
{
    delete this;
    return 0;
}
