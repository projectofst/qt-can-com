#include "COM_LAN.hpp"

LAN::LAN()
{
    Options.append("inUDPoutUDP::0.0.0.0:4001::255.255.255.255:4001");
    Options.append("inTCPoutUDP::127.0.0.1:4000::255.255.255.255:4001");
    Options.append("inUDPoutTCP::127.0.0.1:4000::255.255.255.255:4001");
    Options.append("inTCPoutTCP::127.0.0.1:4000::255.255.255.255:4001");

    in = new UdpReceiveSock;
    out = new UdpSendSock;
    connect(in,SIGNAL(new_messages(CANmessage)),
            this,SIGNAL(new_messages(CANmessage)));

    StatusInt = -1;
}

LAN::~LAN() {}

QStringList LAN::options()
{
    return Options;
}

bool LAN::open(QString Data)
{
    QString Mode = Data.section("::",0,0);
    QMetaObject EnumList = LAN::staticMetaObject;
    OptEnu EnumValue = static_cast<OptEnu>(EnumList.enumerator(
                                  EnumList.indexOfEnumerator(
                                  "OptEnu")).keyToValue(Mode.toLocal8Bit().data()));
    switch (EnumValue) {
    case inUDPoutUDP:
        in->open(Data.section("::",1,1));
        out->open(Data.section("::",2,2));
        StatusInt = 1;
        break;
    default:
        return false;
    }
    return true;
}

int LAN::send(CANmessage msg)
{
    int err = out->send(msg);
    return err;
}

int LAN::close()
{
//    in->close();
//    out->close();
//    StatusInt = 0;
//    return 0;
}
