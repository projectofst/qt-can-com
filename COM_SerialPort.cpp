/**********************************************************************
 *   FST CAN tools --- interface
 *
 *   SerialPort class
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#include <QSerialPort>
#include <QMutex>
#include <QDebug>
#include <qmessagebox.h>


#include <unistd.h>
#include <fcntl.h>
// #include <sys/ioctl.h>
#include <thread>
#include <mutex>
#include <cerrno>

#include "COM_SerialPort.hpp"

#include "can-ids/CAN_IDs.h"

//#define DBG_SERIAL_RECEPTION

#define default_seed 0xFFFF

/**********************************************************************
 * Name:    SerialPort
 * Args:    -
 * Return:  -
 * Desc:    SerialPort class constructor.
 **********************************************************************/
SerialPort::SerialPort(void){
	message_counter=0;
	sum=0;

	_fd = -1;
    StatusInt = 0;

	W_caret = 0;
	R_caret = 0;

	aux_buffer_size = 4096;
	buffer_size 	= 4096*4096;

    buffer = reinterpret_cast<unsigned char*>(malloc (sizeof(unsigned char)*buffer_size)) ;
    if (buffer == nullptr) {
		printf("Can't alloc buffer\n");
		exit(1);
	}
    aux_buffer = reinterpret_cast<char*>(malloc (sizeof(char)*aux_buffer_size));
    if (aux_buffer == nullptr) {
		printf("Can't alloc aux_buffer\n");
		exit(1);
	}

	connect(&serial, SIGNAL(error(QSerialPort::SerialPortError)), this,
        SLOT(handleError(QSerialPort::SerialPortError)));

    connect(&serial, &QSerialPort::readyRead, this, &SerialPort::read);

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(_send()));
    timer->start(10);

	standard_ID_mode();
	return;
}


/**********************************************************************
 * Name:    ~SerialPort
 * Args:    -
 * Return:  -
 * Desc:    SerialPort class destructor.
 **********************************************************************/
SerialPort::~SerialPort(void){
	// clean exit for _worker in case it's still going at it
    close();
	return;
}


/**********************************************************************
 * Name:    HandleError
 * Args:    -
 * Return:  -
 * Desc:    SerialPort class destructor.
 **********************************************************************/

void SerialPort::handleError(QSerialPort::SerialPortError error)
{
	switch (error){
			case QSerialPort::NoError:
				break;
			case QSerialPort::DeviceNotFoundError:
				fprintf(stderr, "Unexpected Error, Device Not Found\n");
				break;
			case QSerialPort::PermissionError:
				fprintf(stderr, "Really Sorry, But you lack the permissions\n");
				break;
			case QSerialPort::OpenError:
				fprintf(stderr, "Unexpected Error while opening the port\n");
				break;
			case QSerialPort::NotOpenError:
				fprintf(stderr, "Unexpected Error is the device open?\n");
				break;
			case QSerialPort::WriteError:
				fprintf(stderr, "Error while writing, closing port...\n");
                close();
				break;
			case QSerialPort::ReadError:
				fprintf(stderr, "Error while reading, closing port...\n");
                close();
				break;
			case QSerialPort::ResourceError:
				fprintf(stderr, "Resource Error. Did you remove the device? closing port...\n");
                close();
				break;
			case QSerialPort::UnsupportedOperationError:
				fprintf(stderr, "Dont you ping of death me!!...\n");
                close();
				break;
			default:
				fprintf(stderr, "I cant handle this, closing port...\n");
                close();
				break;
		}
}

/**********************************************************************
 * Name:    Open
 * Args:    const char *portname, BaudRate
 * Return:  exit status
 * Desc:    Opens and configures the serial port.
 *          Starts worker thread to read from the port.
 **********************************************************************/
bool SerialPort::open(const QString portname)
{
    // reopens port rather than creating another file descriptor every time open is issued
    if (StatusInt != 0){
        close();
	}

	// actual opening
    serial.setPortName(portname);
    serial.setBaudRate(1000000);
    serial.setDataBits(QSerialPort::Data8);
    serial.setParity(QSerialPort::NoParity);
    serial.setStopBits(QSerialPort::OneStop);
    serial.setFlowControl(QSerialPort::NoFlowControl);

	int err = serial.open(QIODevice::ReadWrite);
    if (err) {
        StatusInt = 1;
	}

	return err;
}

/**********************************************************************
 * Name:    close
 * Args:    -
 * Return:  exit status
 * Desc:    Closes serial port, if open. Returns -1 otherwise.
 **********************************************************************/
int SerialPort::close(void){

	int err = 0;


    if(!StatusInt){	// if open
		// mutex.unlock();	// sneaky bastard, but using only one return to avoid the deadlock is easier to read
		// return -1;
		err = -1;
	}else{
        StatusInt = 0;
		serial.close();
	}


	return err;
}

QByteArray SerialPort::pop_raw_data(void){
    return data;
}

/**********************************************************************
 * Name:    remove_white_space
 * Args:    QStringList *CanDataMessage
 * Return:  -
 * Desc:    Removes any blanck fields from a given QStringList.
 ** ********************************************************************/
void remove_white_space(QStringList *CanMessageList_temp){
    for (int k = 0; k <= CanMessageList_temp->size()-1; k++){
        if ((*CanMessageList_temp)[k] == ""){
            CanMessageList_temp->removeAt(k);
        }
    }
}

/**********************************************************************
 * Name:    read
 * Args:    -
 * Return:  -
 * Desc:    Independent thread to read from a serial port.
 ** ********************************************************************/

void SerialPort::read(void)
{
    static uint64_t timestamp = 0;
    int valid_messages = 0;
    memset(aux_buffer, 0, aux_buffer_size);

    // read can't be blocking or thread may never quit before another one is issued!
    // call fcntl with FNDELAY

    while(serial.bytesAvailable()) {
        //_queueLkc.lock();
        data = serial.readLine();
        memcpy(aux_buffer,data.data(),static_cast<size_t>(data.size()));
        //_queueLkc.unlock();
        //fprintf(stdout, "[%s]\n", data.data());
        QString string = data.data();
        auto CanMessageList = string.split(",");
        remove_white_space(&CanMessageList);
        if (this->debug_status == 1){
            emit new_raw();
            continue;
        }

        CANmessage canmessage;
        CANdata msg;
        int serial_size = CanMessageList.size();
        if (serial_size == 6) {
            valid_messages++;
            canmessage.timestamp = timestamp++;
        }
        else if (serial_size == 7) {
            valid_messages++;
        }
        else {
            continue;
        }



        msg.sid = CanMessageList.at(0).toInt();
        msg.dlc = CanMessageList.at(1).toInt();

        for (int i=0; i<4; i++) {
            msg.data[i] = 0;
        }

        for (int i=0; i<4; i++) {
            msg.data[i] = CanMessageList.at(i+2).toInt();
        }

        canmessage.candata = msg;
        //canmessage.timestamp = CanMessageList.at(6).toUShort();
        //_queueLkc.lock();
        _queue.push(canmessage);
        //_queueLkc.unlock();

        if (serial_size == 7) {
            canmessage.timestamp = CanMessageList.at(6).toInt();
        }

        if (valid_messages) {
            emit new_messages(canmessage);
            continue;
        }
    }
}


//Thanks friend: https://stackoverflow.com/a/12386915
// Yet, another good itoa implementation
//
// returns: the length of the number string
uint16_t base10_to_string(uint8_t *sp, int value)
{
    uint8_t tmp[16];// be careful with the length of the buffer
    uint8_t *tp = tmp;
    int i;
    unsigned v;
    int radix = 10;

    int sign = (radix == 10 && value < 0);
    if (sign)
        v = -value;
    else
        v = (unsigned)value;

    while (v || tp == tmp)
    {
        i = v % radix;
        v /= radix; // v/=radix uses less CPU clocks than v=v/radix does
        if (i < 10)
            *tp++ = i+'0';
        else
            *tp++ = i + 'a' - 10;
    }

    int len = tp - tmp;

    if (sign)
    {
        *sp++ = '-';
        len++;
    }

    while (tp > tmp)
        *sp++ = *--tp;

    return len;
}

int SerialPort::send(CANmessage msg)
{
    send_queue.enqueue(msg);
    return 0;
}

/**********************************************************************
 * Name:    send
 * Args:    CANdata msg
 * Return:  exit status
 * Desc:    Send CAN message to UART.
 **********************************************************************/
void SerialPort::_send()
{
    if (send_queue.empty()) {
        return;
    }

    CANmessage msg = this->send_queue.dequeue();
    uint8_t message[256];
    uint8_t aux[256];
    uint16_t i=0, len=0;

    len = base10_to_string(aux, msg.candata.sid);
    strcpy(reinterpret_cast<char *>(message+i),reinterpret_cast<char *>(aux));
    i += len;
    message[i++] = ',';
    len = base10_to_string(aux, msg.candata.dlc);
    strcpy(reinterpret_cast<char *>(message+i),reinterpret_cast<char *>(aux));
    i+=len;
    message[i++] = ',';
    len = base10_to_string(aux, msg.candata.data[0]);
    strcpy(reinterpret_cast<char *>(message+i),reinterpret_cast<char *>(aux));
    i+=len;
    message[i++] = ',';
    len = base10_to_string(aux, msg.candata.data[1]);
    strcpy(reinterpret_cast<char *>(message+i),reinterpret_cast<char *>(aux));
    i+=len;
    message[i++] = ',';
    len = base10_to_string(aux, msg.candata.data[2]);
    strcpy(reinterpret_cast<char *>(message+i),reinterpret_cast<char *>(aux));
    i+=len;
    message[i++] = ',';
    len = base10_to_string(aux, msg.candata.data[3]);
    strcpy(reinterpret_cast<char *>(message+i),reinterpret_cast<char *>(aux));
    i+=len;
    message[i] = '\n';
    message[i+1] = '\0';

    _writePort(reinterpret_cast<const unsigned char *>(message), i+1);
}


/**********************************************************************
 * Name:    writePort
 * Args:    const unsigned char *data, unsigned int n
 * Return:  exit status
 * Desc:    Send byte sequence to UART.
 **********************************************************************/
int SerialPort::_writePort(const unsigned char *data, unsigned int n)
{
    if(!StatusInt){
        return -1;
    }

    if(serial.write(const_cast<char *>(reinterpret_cast<const char *>(data)), static_cast<qint64>(n))!=n){
        return -1;
    }

    return 0;
}


/**********************************************************************
 * Name:    extended_ID_mode
 * Args:    -
 * Return:  -
 * Desc:    Change mode to use extended IDs.
 *          Value indicates the amount of bytes to parse more.
 **********************************************************************/
void SerialPort::extended_ID_mode(void){
    _use_extID = 3;		// the number of bytes of the extID
}


/**********************************************************************
 * Name:    standard_ID_mode
 * Args:    -
 * Return:  -
 * Desc:    Change mode to use the standard ID only.
 **********************************************************************/
void SerialPort::standard_ID_mode(void){
    _use_extID = 0;
}


/**********************************************************************
 * Name:    pop_message
 * Args:    -
 * Return:  -
 * Desc:    Returns and pops from internal queue one CAN message
 **********************************************************************/
CANmessage SerialPort::pop(void){

    CANmessage package;

    if(!_queue.empty()){
        package = _queue.front();
        _queue.pop();
    }
    return package;
}


/**********************************************************************
 * Name:    _CRC_calculate
 * Args:    const unsigned char *data, const unsigned int index,
 *                      const unsigned int buffersize, unsigned int n
 * Return:  CRC byte
 * Desc:    Calculates CRC for n byte data in a circular buffer from
 *          index onwards.
 **********************************************************************/
unsigned char SerialPort::_CRC_calculate(const unsigned char *data, const unsigned int index, const unsigned int buffersize, unsigned int n){

    unsigned int i, j;

    unsigned char CRC = 0b01000001;
    unsigned char DIN, IN0, IN1, IN2;

    unsigned char aux;

    for(j=0;j<n;j++){
        aux = 0b10000000;
        for(i=0;i<8;i++){		/* shift the whole byte */

            DIN = (data[(index+j)%buffersize] & aux) >> (7-i);	/* isolates the ith MSB from byte on LSB position */

            IN0 = DIN ^ ((CRC & 0b10000000)>>7);	/* DIN XOR CRC[7] */
            IN1 = (CRC & 0b00000001) ^ IN0;			/* CRC[0] XOR IN0 */
            IN2 = ((CRC & 0b00000010)>>1) ^ IN0;	/* CRC[1] XOR IN0 */

            CRC = ((CRC << 1) & 0b11111000) | IN2<<2 | IN1<<1 | IN0;	/* shifts CRC 1 bit left and assigns 3 LSBs to IN2,IN1,IN0 */

            aux = aux >> 1;		/* shifts one bit from byte */
        }
    }
    return CRC;
}


/**********************************************************************
 * Name:    _CRC_verify
 * Args:    const unsigned char *data, const unsigned int index,
 *             const unsigned int buffersize, unsigned int n,
 *             const unsigned char CRC
 * Return:  exit status
 * Desc:    Verifies if CRC is correct for n byte data in a circular
 *          buffer from index onwards.
 **********************************************************************/
int SerialPort::_CRC_verify(const unsigned char *data, const unsigned int index, const unsigned int buffersize, unsigned int n, const unsigned char CRC){

    if(_CRC_calculate(data, index, buffersize, n) == CRC){
        return 0;
    }

    return -1;
}

void SerialPort::set_message_to_queue(CANmessage msg){
    _queue.push(msg);
    emit new_messages(msg);
}

QStringList SerialPort::options(void)
{
    Options.clear();
    if (StatusInt <= 0) {
    foreach (const QSerialPortInfo &port, QSerialPortInfo::availablePorts())
        Options.append(port.portName());
    } else {
        Options.append("(no options)");
    }
    return Options;
}
