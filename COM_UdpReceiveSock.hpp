#ifndef UDPRECEIVESOCK_HPP
#define UDPRECEIVESOCK_HPP

#include "COM.hpp"

#include <QHostAddress>

class UdpReceiveSock : public COM
{
    Q_OBJECT

public:
    UdpReceiveSock();
    virtual bool open(QString) override;
    virtual int close(void) override;

public slots:
    virtual int send(CANmessage) override;
    virtual void read(void) override;

private:
    QQueue<QString> MsgList;
    QUdpSocket *UdpSocket;
    QByteArray datagram;

signals:
    void new_messages(CANmessage);
};

#endif // UDPRECEIVESOCK_HPP

