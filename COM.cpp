#include "COM.hpp"

COM::COM()
{
    Status = 0;
}

int COM::status(void)
{
    return Status;
}

bool COM::open(const QString)
{
    Status = 1;
    return true;
}

int COM::close(void)
{
    Status = -1;
    delete this;
    return 0;
}

int COM::send(CANmessage)
{
    return 0;
}

CANmessage COM::pop(void)
{
    CANmessage package;
    return package;

}

void COM::read(void)
{
    return;

}

QStringList COM::options(void)
{
    return QStringList();
}
