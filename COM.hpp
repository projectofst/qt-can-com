#ifndef COM_H
#define COM_H

#include <QObject>
#include <QSerialPort>
#include <QTcpSocket>
#include <QTcpServer>
#include <QtNetwork>
#include <QNetworkSession>
#include <QTimer>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <queue>
#include <thread>
#include <mutex>
#include <QMutex>
#include <QUdpSocket>

#include "can-ids/CAN_IDs.h"

namespace Ui {
    class COM;
}

class COM : public QObject
{
    Q_OBJECT

public:
    COM();
    int Status;
    virtual QStringList options(void);
    virtual bool open(QString);
    virtual int close(void);
    virtual int send(CANmessage);
    virtual CANmessage pop(void);
    virtual int status(void);

public slots:
    virtual void read(void);

private:
    QMap<QString,COM*> Interfaces;
    QMap<QString,COM*> Loggers;
    
signals:
    void new_messages(CANmessage);
    void broadcast_in_message(CANmessage);
};

#endif // COM_H
