#ifndef COM_LAN_HPP
#define COM_LAN_HPP

#include <QObject>

#include "COM.hpp"
#include "COM_UdpReceiveSock.hpp"
#include "COM_UdpSendSock.hpp"

class LAN : public COM
{
    Q_OBJECT

public:
    enum OptEnu {
        inUDPoutUDP = 1,
        inTCPoutUDP = 2,
        inUDPoutTCP = 3,
        inTCPoutTCP = 4
    }; Q_ENUM(OptEnu)

    LAN();
    virtual ~LAN() override;
    virtual QStringList options() override;
        QStringList Options;
    virtual int status() override {return StatusInt;}
        int StatusInt;
    virtual bool open(const QString) override;
    virtual int  close(void) override;

signals:
    void new_messages(CANmessage);

public slots:
    virtual int send(CANmessage) override;

private:
    COM *in;
    COM *out;

};

#endif // COM_LAN_HPP
